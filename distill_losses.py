import torchmetrics
from torchmetrics.functional import kl_divergence
import torch
from pytorch_metric_learning.trainers.base_trainer import BaseTrainer
from pytorch_metric_learning.utils import common_functions as c_f

DEVICE = torch.device("cuda:0")


def l2_loss(teacher_embs, strudent_embs):
  return torch.norm(teacher_embs - strudent_embs)


def distill_loss(emb_s, emb_t, alpha = 1.):
  return alpha * torch.norm(torch.cov(emb_s) - torch.cov(emb_t))


def fid_base_dist_loss(teacher_embs, strudent_embs):
  m_t = teacher_embs.mean(dim=0)
  std_t = teacher_embs.std(dim=0)
  m_s = strudent_embs.mean(dim=0)
  std_s = strudent_embs.std(dim=0)
  return ((m_t-m_s)**2).sum() + std_t.sum() + std_s.sum() - 2 * torch.sqrt(std_t*std_s).sum()


def kd_loss(teacher_embs, strudent_embs, T = 4):
  sfm_func = torch.nn.Softmax(dim=1)
  sfm_t = sfm_func(teacher_embs / T)
  sfm_s = sfm_func(strudent_embs / T)
  return kl_divergence(sfm_t, sfm_s)


def match_loss(teacher_embs, strudent_embs):
  return ((((teacher_embs[0] - teacher_embs)**2).sum(dim=1) - ((strudent_embs[0] - strudent_embs)**2).sum(dim=1))**2).sum()


def mean_dist_loss(teacher_embs, strudent_embs):
  def get_batch_pairwise_dist(embs):
    return (((embs[None, :, :] - embs[:,None, :])**2).sum(dim=2)**0.5)
  teacher_pw_dist = get_batch_pairwise_dist(teacher_embs)
  student_pw_dist = get_batch_pairwise_dist(strudent_embs)
  mu = teacher_pw_dist.mean() + 1e-5
  return torch.nn.functional.huber_loss(teacher_pw_dist / mu, student_pw_dist / mu)



class MetricLossOnly(BaseTrainer):
    def calculate_loss(self, curr_batch):
        
        data, labels = curr_batch
        
        labels =  torch.tensor(labels, dtype=torch.int64).to(DEVICE)
        embeddings = self.compute_embeddings(data)
        indices_tuple = self.maybe_mine_embeddings(embeddings, labels)
        self.losses["metric_loss"] = self.maybe_get_metric_loss(
            embeddings, labels, indices_tuple
        )

    def maybe_get_metric_loss(self, embeddings, labels, indices_tuple):
        if self.loss_weights.get("metric_loss", 0) > 0:
            return self.loss_funcs["metric_loss"](embeddings, labels, indices_tuple)
        return 0


class BaseTrainerDistill(BaseTrainer):
    def get_batch(self):
         self.dataloader_iter, curr_batch = c_f.try_next_on_generator(self.dataloader_iter, self.dataloader)
         data, labels, embeddings_t = self.data_and_label_getter(curr_batch)
         labels = c_f.process_label(labels, self.label_hierarchy_level, self.label_mapper)
         return data, labels, embeddings_t


class MetricLossWithDistillation(BaseTrainerDistill):
    def __init__(
        self,
        models,
        optimizers,
        batch_size,
        loss_funcs,
        mining_funcs,
        dataset,
        iterations_per_epoch=None,
        data_device=None,
        dtype=None,
        loss_weights=None,
        sampler=None,
        collate_fn=None,
        lr_schedulers=None,
        gradient_clippers=None,
        freeze_these=(),
        freeze_trunk_batchnorm=False,
        label_hierarchy_level=0,
        dataloader_num_workers=2,
        data_and_label_getter=None,
        dataset_labels=None,
        set_min_label_to_zero=False,
        end_of_iteration_hook=None,
        end_of_epoch_hook=None,
        distill_loss = None,
        distillation_weight = 0
    ):
        self.models = models
        self.optimizers = optimizers
        self.batch_size = batch_size
        self.loss_funcs = loss_funcs
        self.mining_funcs = mining_funcs
        self.dataset = dataset
        self.iterations_per_epoch = iterations_per_epoch
        self.data_device = data_device
        self.dtype = dtype
        self.sampler = sampler
        self.collate_fn = collate_fn
        self.lr_schedulers = lr_schedulers
        self.gradient_clippers = gradient_clippers
        self.freeze_these = freeze_these
        self.freeze_trunk_batchnorm = freeze_trunk_batchnorm
        self.label_hierarchy_level = label_hierarchy_level
        self.dataloader_num_workers = dataloader_num_workers
        self.loss_weights = loss_weights
        self.data_and_label_getter = data_and_label_getter
        self.dataset_labels = dataset_labels
        self.set_min_label_to_zero = set_min_label_to_zero
        self.end_of_iteration_hook = end_of_iteration_hook
        self.end_of_epoch_hook = end_of_epoch_hook
        self.loss_names = list(self.loss_funcs.keys())
        self.distill_loss = distill_loss
        self.distillation_weight = distillation_weight
        self.custom_setup()
        self.verify_dict_keys()
        self.initialize_models()
        self.initialize_data_device()
        self.initialize_label_mapper()
        self.initialize_loss_tracker()
        self.initialize_loss_weights()
        self.initialize_data_and_label_getter()
        self.initialize_hooks()
        self.initialize_lr_schedulers()

    def calculate_loss(self, curr_batch):
        
        data, labels, embeddings_t = curr_batch

        embeddings_t = torch.tensor(embeddings_t).to(DEVICE)
        
        labels =  torch.tensor(labels, dtype=torch.int64).to(DEVICE)
        embeddings = self.compute_embeddings(data)
        indices_tuple = self.maybe_mine_embeddings(embeddings, labels)

        ############ место для дистилл лосса #############
        distill_loss = self.distillation_weight * self.distill_loss(embeddings_t, embeddings)
        ############ место для дистилл лосса #############


        self.losses["metric_loss"] = self.maybe_get_metric_loss(
            embeddings, labels, indices_tuple
        ) + distill_loss

        

    def maybe_get_metric_loss(self, embeddings, labels, indices_tuple):
        if self.loss_weights.get("metric_loss", 0) > 0:
            return self.loss_funcs["metric_loss"](embeddings, labels, indices_tuple)
        return 0