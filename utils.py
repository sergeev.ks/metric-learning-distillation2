from PIL import Image
import pickle
from powerful_benchmarker import architectures
from pytorch_metric_learning.utils import common_functions as c_f
import torch
import os
from torch.utils.data import Dataset
import logging
from torchvision import datasets
import numpy as np
from tqdm import tqdm
import scipy.io as sio
from torchvision import transforms


class ConvertToBGR(object):
    """
    Converts a PIL image from RGB to BGR
    """
    def __init__(self):
        pass
    def __call__(self, img):
        r, g, b = img.split()
        img = Image.merge("RGB", (b, g, r))
        return img
    def __repr__(self):
        return "{}()".format(self.__class__.__name__)


class Multiplier(object):
    def __init__(self, multiple):
        self.multiple = multiple
    def __call__(self, img):
        return img*self.multiple
    def __repr__(self):
        return "{}(multiple={})".format(self.__class__.__name__, self.multiple)


TRANSFORM = transforms.Compose([ConvertToBGR(),
                                transforms.Resize(256), 
                                transforms.RandomResizedCrop(scale = (0.16, 1), ratio= (0.75, 1.33), size = 227),
                                transforms.RandomHorizontalFlip(0.5),
                                transforms.ToTensor(),
                                Multiplier(255),
                                transforms.Normalize(mean = [104, 117, 128], 
                                                     std = [1, 1, 1])])


class Cars196(Dataset):
    def __init__(self, root, transform=TRANSFORM, download=False, teacher = None):
        self.root = os.path.join(root, "cars196")
        self.set_paths_and_labels()
        self.transform = transform

    def __len__(self):
        return len(self.labels)

    def __getitem__(self, idx):
        path = self.img_paths[idx]
        img = Image.open(path).convert("RGB")
        
        label = self.labels[idx]
        
        if self.transform is not None:
            img = self.transform(img)

        return img, label

    def load_labels(self):
        img_data = sio.loadmat(os.path.join(self.dataset_folder, "cars_annos.mat"))
        self.labels = np.array([i[0, 0] for i in img_data["annotations"]["class"][0]]) - 1
        self.img_paths = [os.path.join(self.dataset_folder, i[0]) for i in img_data["annotations"]["relative_im_path"][0]]
        self.class_names = [i[0] for i in img_data["class_names"][0]]

    def set_paths_and_labels(self, assert_files_exist=False):
        self.dataset_folder = self.root
        self.load_labels()
        assert len(np.unique(self.labels)) == 196
        assert self.__len__() == 16185
        if assert_files_exist:
            logging.info("Checking if dataset images exist")
            for x in tqdm.tqdm(self.img_paths):
                assert os.path.isfile(x)



class CUB200(Dataset):
    def __init__(self, root, transform=TRANSFORM, download=False):
        self.root = os.path.join(root, "cub2011")
        self.set_paths_and_labels()
        self.transform = transform

    def __len__(self):
        return len(self.dataset)

    def __getitem__(self, idx):
        img, label = self.dataset[idx]
        if self.transform is not None:
            img = self.transform(img)

        return img, label

    def set_paths_and_labels(self):
        img_folder = os.path.join(self.root, "CUB_200_2011", "images")
        self.dataset = datasets.ImageFolder(img_folder)
        self.labels = np.array([b for (a, b) in self.dataset.imgs])
        assert len(np.unique(self.labels)) == 200
        assert self.__len__() == 11788
        
        
class JointDataset(Dataset):
    def __init__(self, root_cars, root_cub, transform = TRANSFORM, teachers_embeddings = None):
        self.root_cub = os.path.join(root_cub, "cub2011")
        self.root_cars = os.path.join(root_cars, "cars196")
        self.set_paths_and_labels_cars()
        self.set_paths_and_labels_cub()
        self.set_labels()
        self.transform = transform
        self.teachers_embeddings = teachers_embeddings


    def __len__(self):
        return len(self.labels)
    
    def __getitem__(self, idx):
        if idx < len(self.labels_car):
            return self.getitem_car(idx)
        else:
            return self.getitem_cub(idx - len(self.labels_car))


    def getitem_cub(self, idx):
        img, label = self.dataset_cub[idx]
        if self.transform is not None:
            img = self.transform(img)
        
        if self.teachers_embeddings is not None:
          return img, label+196, self.teachers_embeddings['cub'][idx]

        return img, label+196


    def getitem_car(self, idx):
        path = self.img_paths[idx]
        img = Image.open(path).convert("RGB")
        label = self.labels_car[idx]
        
        if self.transform is not None:
            img = self.transform(img)
        
        if self.teachers_embeddings is not None:
          return img, label, self.teachers_embeddings['cars'][idx]

        return img, label


    def set_paths_and_labels_cub(self):
        img_folder = os.path.join(self.root_cub, "CUB_200_2011", "images")
        self.dataset_cub = datasets.ImageFolder(img_folder)
        self.labels_cub = np.array([b for (a, b) in self.dataset_cub.imgs]) + 196
      

    def load_labels(self):
        img_data = sio.loadmat(os.path.join(self.dataset_folder_cars, "cars_annos.mat"))
        self.labels_car = np.array([i[0, 0] for i in img_data["annotations"]["class"][0]]) - 1
        self.img_paths = [os.path.join(self.dataset_folder_cars, i[0]) for i in img_data["annotations"]["relative_im_path"][0]]
        self.class_names = [i[0] for i in img_data["class_names"][0]]


    def set_paths_and_labels_cars(self, assert_files_exist=False):
        self.dataset_folder_cars = self.root_cars
        self.load_labels()
        
        
    def set_labels(self):
        self.labels = np.concatenate([self.labels_car, self.labels_cub], axis = 0)


def get_train_val_test_datasets(dataset, train_idx, val_idx):
    
    train_set = torch.utils.data.Subset(dataset, train_idx)
    train_set.labels = dataset.labels[train_idx]
    val_set = torch.utils.data.Subset(dataset, val_idx)
    val_set.labels = dataset.labels[val_idx]
        
    test_idx = [i for i in range(len(dataset)) if (i not in train_idx) and (i not in val_idx)]
    
    test_set = torch.utils.data.Subset(dataset, test_idx)
    test_set.labels = dataset.labels[test_idx]
    
    return train_set, val_set, test_set


def get_pretrained_metric_learning_model(path_to_trunk, path_to_embedder):
    trunk = pickle.load(open('bninception.pickle', 'rb'))
    trunk.last_linear = c_f.Identity()
    embedder = architectures.misc_models.MLP([1024, 128])

    trunk.load_state_dict(torch.load(path_to_trunk))
    embedder.load_state_dict(torch.load(path_to_embedder))


    return  {"trunk": trunk, "embedder": embedder}