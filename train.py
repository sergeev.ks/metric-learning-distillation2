from pytorch_metric_learning.trainers.base_trainer import BaseTrainer
import torch
import pickle
from pytorch_metric_learning.utils import common_functions as c_f
from powerful_benchmarker import architectures
from pytorch_metric_learning import losses, miners, samplers, testers, trainers
import pytorch_metric_learning.utils.logging_presets as LP


DEVICE = torch.device("cuda:0")


def get_tester_and_hook(dataset_dict, name_of_set):
    log_folder, tensorboard_folder = f"{name_of_set}/logs", f"{name_of_set}/tensorboard"
    record_keeper, _, _ = LP.get_record_keeper(log_folder, tensorboard_folder)
    hooks = LP.get_hook_container(record_keeper)
    model_folder = f"{name_of_set}/saved_models"

    tester = testers.GlobalEmbeddingSpaceTester(end_of_testing_hook=hooks.end_of_testing_hook)

    end_of_epoch_hook = hooks.end_of_epoch_hook(tester,
                                                dataset_dict,
                                                model_folder,
                                                test_interval=3,
                                                patience=9)
    
    return tester, end_of_epoch_hook


def train_t(train_set, val_set, loss_func, emb_size, name_of_set, trainer, end_of_epoch_hook, distill_loss = None, distillation_weight = 0):
    mining_funcs = dict()
    
    loss_func = loss_func.to(DEVICE)

    trunk = pickle.load(open('bninception.pickle', 'rb'))

    trunk_output_size = trunk.last_linear.in_features
    trunk.last_linear = c_f.Identity()
    embedder = architectures.misc_models.MLP([trunk_output_size, emb_size])
    
    trunk = trunk.to(DEVICE)
    embedder = embedder.to(DEVICE)
    

    trunk_optimizer = torch.optim.RMSprop(trunk.parameters(), lr=1e-6, momentum=0.9, weight_decay=0.0001)
    embedder_optimizer = torch.optim.RMSprop(embedder.parameters(), lr=1e-6, momentum=0.9, weight_decay=0.0001)
    
    
    sampler = samplers.MPerClassSampler(
    train_set.labels, m=4
    )

    batch_size = 8
    num_epochs = 1000

    models = {"trunk": trunk, "embedder": embedder}
    optimizers = {
        "trunk_optimizer": trunk_optimizer,
        "embedder_optimizer": embedder_optimizer
    }
    loss_funcs = {"metric_loss": loss_func}
        
    if distill_loss is not None:
      return trainer(
          models,
          optimizers,
          batch_size,
          loss_funcs,
          mining_funcs,
          train_set,
          sampler=sampler,
          iterations_per_epoch = 100,
          freeze_trunk_batchnorm = True,
          label_hierarchy_level = 0,
          loss_weights = None,
          dataloader_num_workers = 0,
          end_of_epoch_hook=end_of_epoch_hook,
          distill_loss = distill_loss,
          distillation_weight = distillation_weight
      )
    else:
      return trainer(
          models,
          optimizers,
          batch_size,
          loss_funcs,
          mining_funcs,
          train_set,
          sampler=sampler,
          iterations_per_epoch = 100,
          freeze_trunk_batchnorm = True,
          label_hierarchy_level = 0,
          loss_weights = None,
          dataloader_num_workers = 0,
          end_of_epoch_hook=end_of_epoch_hook
      )